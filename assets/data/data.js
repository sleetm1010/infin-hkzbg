var animals_data = [ 
	{
		'id':'1',
		'url':'animals/mammals/animals_10.html',
		'type':'mammals',
		'image':'Mammals/De_Brazzas_Monkey.jpg',
		'en':{
			'name':"De Brazza's Monkey",
		},
		'tc':{
			'name':'白臀長尾猴',
		},
		'sc':{
			'name':'白臀长尾猴',
		},
	},
	{
		'id':'2',
		'url':'animals/mammals/animals_4.html',
		'type':'mammals',
		'image':'Mammals/Buff_cheeked_Gibbon.jpg',
		'en':{
			'name':"Buff-cheeked Gibbon",
		},
		'tc':{
			'name':'紅頰黑猿',
		},
		'sc':{
			'name':'红颊黑猿',
		},
	},
	{
		'id':'3',
		'url':'animals/mammals/animals_1.html',
		'type':'mammals',
		'image':'Mammals/Siamang_4.jpg',
		'en':{
			'name':"Siamang",
		},
		'tc':{
			'name':'合趾猿',
		},
		'sc':{
			'name':'合趾猿',
		},
	},
	{
		'id':'4',
		'url':'animals/mammals/animals_8.html',
		'type':'mammals',
		'image':'Mammals/Ring_tailed_Lemur.jpg',
		'en':{
			'name':"Ring-tailed Lemur",
		},
		'tc':{
			'name':'環尾狐猴',
		},
		'sc':{
			'name':'环尾狐猴',
		},
	},
	{
		'id':'5',
		'url':'animals/mammals/animals_2.html',
		'type':'mammals',
		'image':'Mammals/Golden_headed_Lion_Tamarin.jpg',
		'en':{
			'name':"Golden-headed Lion Tamarin",
		},
		'tc':{
			'name':'金頭獅狨猴',
		},
		'sc':{
			'name':'金头狮狨猴',
		},
	},
	{
		'id':'6',
		'url':'animals/mammals/animals_13.html',
		'type':'mammals',
		'image':'Mammals/Golden_Lion_Tamarin.jpg',
		'en':{
			'name':"Golden Lion Tamarin",
		},
		'tc':{
			'name':'獅狨猴',
		},
		'sc':{
			'name':'狮狨猴',
		},
	},
	{
		'id':'7',
		'url':'animals/mammals/animals_6.html',
		'type':'mammals',
		'image':'Mammals/White_faced_Saki.jpg',
		'en':{
			'name':"White-faced Saki",
		},
		'tc':{
			'name':'白面僧面猴',
		},
		'sc':{
			'name':'白面僧面猴',
		},
	},
	{
		'id':'8',
		'url':'animals/mammals/animals_5.html',
		'type':'mammals',
		'image':'Mammals/Bornean_Orangutan.jpg',
		'en':{
			'name':"Bornean Orangutan",
		},
		'tc':{
			'name':'婆羅洲猩猩',
		},
		'sc':{
			'name':'婆罗洲猩猩',
		},
	},
	{
		'id':'9',
		'url':'animals/mammals/animals_3.html',
		'type':'mammals',
		'image':'Mammals/Emperor_Tamarin.jpg',
		'en':{
			'name':"Emperor Tamarin",
		},
		'tc':{
			'name':'皇狨猴',
		},
		'sc':{
			'name':'皇狨猴',
		},
	},
	{
		'id':'10',
		'url':'animals/mammals/animals_14.html',
		'type':'mammals',
		'image':'Mammals/Cotton_top_Tamarin.jpg',
		'en':{
			'name':"Cotton-top Tamarin",
		},
		'tc':{
			'name':'棉頂狨猴',
		},
		'sc':{
			'name':'棉顶狨猴',
		},
	},
	{
		'id':'11',
		'url':'animals/mammals/animals_11.html',
		'type':'mammals',
		'image':'Mammals/Common_Squirrel_Monkey.jpg',
		'en':{
			'name':"Common Squirrel Monkey",
		},
		'tc':{
			'name':'松鼠猴',
		},
		'sc':{
			'name':'松鼠猴',
		},
	},
	{
		'id':'12',
		'url':'animals/mammals/animals_9.html',
		'type':'mammals',
		'image':'Mammals/Black_White_Ruffed_Lemur.jpg',
		'en':{
			'name':"Black & White Ruffed Lemur",
		},
		'tc':{
			'name':'領狐猴',
		},
		'sc':{
			'name':'领狐猴',
		},
	},
	{
		'id':'13',
		'url':'animals/mammals/animals_12.html',
		'type':'mammals',
		'image':'Mammals/Meerkat_Slender_tailed_Meerkat.jpg',
		'en':{
			'name':"Meerkat, Slender-tailed Meerkat",
		},
		'tc':{
			'name':'狐獴',
		},
		'sc':{
			'name':'狐獴',
		},
	},
	{
		'id':'14',
		'url':'animals/mammals/animals_7.html',
		'type':'mammals',
		'image':'Mammals/Hoffmann_Two_toed_Sloth.jpg',
		'en':{
			'name':"Hoffmann's Two-toed Sloth",
		},
		'tc':{
			'name':'霍氏樹懶',
		},
		'sc':{
			'name':'霍氏树懒',
		},
	},
	{
		'id':'15',
		'url':'animals/mammals/animals_15.html',
		'type':'mammals',
		'image':'Mammals/Asian_small_clawed_otter.jpg',
		'en':{
			'name':"Asian small-clawed otter",
		},
		'tc':{
			'name':'亞洲小爪水獺',
		},
		'sc':{
			'name':'亚洲小爪水獭',
		},
	},
	{
		'id':'16',
		'url':'animals/reptiles/animals_21.html',
		'type':'reptiles',
		'image':'Reptiles/Radiated_Tortoise.jpg',
		'en':{
			'name':'Radiated Tortoise',
		},
		'tc':{
			'name':'輻紋陸龜',
		},
		'sc':{
			'name':'辐纹陆龟',
		},
	},
	{
		'id':'17',
		'url':'animals/reptiles/animals_22.html',
		'type':'reptiles',
		'image':'Reptiles/African_Spurred_Tortoise.jpg',
		'en':{
			'name':'African Spurred Tortoise',
		},
		'tc':{
			'name':'盾臂龜',
		},
		'sc':{
			'name':'盾臂龟',
		},
	},
	{
		'id':'18',
		'url':'animals/reptiles/animals_23.html',
		'type':'reptiles',
		'image':'Reptiles/Elongated_Tortoise.jpg',
		'en':{
			'name':'Elongated Tortoise',
		},
		'tc':{
			'name':'長殼龜',
		},
		'sc':{
			'name':'长壳龟',
		},
	},
	{
		'id':'19',
		'url':'',
		'popup':true,
		'dataID':'reptiles19',
		'type':'reptiles',
		'status':'LC',
		'image':'Reptiles/Leopard_Tortoise.jpg',
		'en':{
			'name':'Leopard Tortoise',
			'scientific':'Geochelone pardalis',
			'Class':'Reptilia',
			'order': 'Testudines',
			'distribution':'East and South Africa',
			'video':'https://www.youtube.com/watch?v=Io5aTVKpduU',
		},
		'tc':{
			'name':'豹紋龜',
			'scientific':'Geochelone pardalis',
			'Class':'爬行綱',
			'order': '龜鱉目',
			'distribution':'非洲東部及南部',
			'video':'https://www.youtube.com/watch?v=VRu8x5HPvvM',
		},
		'sc':{
			'name':'豹纹龟',
			'scientific':'Geochelone pardalis',
			'Class':'爬行纲',
			'order': '龟鳖目',
			'distribution':'非洲东部及南部',
			'video':'https://www.youtube.com/watch?v=1PGZeh2iCEM',
		},
	},
	{
		'id':'20',
		'url':'',
		'popup':true,
		'dataID':'reptiles20',
		'type':'reptiles',
		'status':'LC',
		'image':'Reptiles/Red_eared_slider.jpg',
		'en':{
			'name':'Red-eared slider',
			'scientific':'Trachemys scripta elegans',
			'Class':'Reptilia',
			'order': 'Testudines',
			'distribution':'North America',
		},
		'tc':{
			'name':'紅耳龜',
			'scientific':'Trachemys scripta elegans',
			'Class':'爬行綱',
			'order': '龜鱉目',
			'distribution':'美國北部',
		},
		'sc':{
			'name':'红耳龟',
			'scientific':'Trachemys scripta elegans',
			'Class':'爬行纲',
			'order': '龟鳖目',
			'distribution':'美国北部',
		},
	},
	{
		'id':'21',
		//'url':'animals/birds/animals_38.html',
		'url':'',
		'type':'birds',
		'popup':true,
		'dataID':'bird21',
		'image':'Birds/Wood_Duck.jpg',
		'status':'LC',
		'en':{
			'name':'Wood Duck',
			'scientific':'Aix sponsa',
			'Class':'Aves',
			'order': 'Anseriformes',
			'distribution':'Southern Canada, U.S.A., Cuba',
		},
		'tc':{
			'name':'林鴛鴦',
			'scientific':'Aix sponsa',
			'Class':'鳥綱',
			'order': '雁形目',
			'distribution':'加拿大南部，美國，古巴',
		},
		'sc':{
			'name':'林鸳鸯',
			'scientific':'Aix sponsa',
			'Class':'鸟纲',
			'order': '雁形目',
			'distribution':'加拿大南部，美国，古巴',
		},
	},
	{
		'id':'22',
		//'url':'animals/birds/animals_54.html',
		'url':'',
		'type':'birds',
		'popup':true,
		'dataID':'bird22',
		'image':'Birds/Blue_Fronted_Amazon_Parrot.jpg',
		'status':'NT',
		'en':{
			'name':'Blue-Fronted Amazon Parrot',
			'scientific':'Amazona aestiva',
			'Class':'Aves',
			'order': 'Psittaciformes',
			'distribution':'N.E. Brazil to Paraguay and N. Argentina',
		},
		'tc':{
			'name':'藍頭亞瑪遜鸚鵡',
			'scientific':'Amazona aestiva',
			'Class':'鳥綱',
			'order': '鸚形目',
			'distribution':'巴西東北部至巴拉圭和阿根廷北部',
		},
		'sc':{
			'name':'蓝头亚玛逊鹦鹉',
			'scientific':'Amazona aestiva',
			'Class':'鸟纲',
			'order': '鹦形目',
			'distribution':'巴西东北部至巴拉圭和阿根廷北部',
		},
	},
	{
		'id':'23',
		'url':'',
		'popup':true,
		'dataID':'bird23',
		'type':'birds',
		'image':'Birds/Blacksmith_Plover.jpg',
		'status':'LC',
		'en':{
			'name':'Blacksmith Plover',
			'scientific':'Anitibyx armatus',
			'Class':'Aves',
			'order': 'Charadriiformes',
			'distribution':'Angola, C. Tanzania. C.S. Kenya & South Africa, expect W. Coast of Namibia',
		},
		'tc':{
			'name':'黑枕麥雞',
			'scientific':'Anitibyx armatus',
			'Class':'鳥綱',
			'order': '鴴形目',
			'distribution':'安哥拉，坦桑尼亞中部，肯亞中南部及南非（納米比亞西岸除外）',
		},
		'sc':{
			'name':'黑枕麦鸡',
			'scientific':'Anitibyx armatus',
			'Class':'鸟纲',
			'order': '鸻形目',
			'distribution':'安哥拉，坦桑尼亚中部，肯亚中南部及南非（纳米比亚西岸除外）',
		},
	},
	{
		'id':'24',
		'url':'animals/birds/animals_35.html',
		'type':'birds',
		'image':'Birds/Blue_Crane.jpg',
		'en':{
			'name':'Blue Crane',
		},
		'tc':{
			'name':'藍鶴',
		},
		'sc':{
			'name':'蓝鹤',
		},
	},
	{
		'id':'25',
		'url':'',
		'popup':true,
		'dataID':'bird25',
		'type':'birds',
		'image':'Birds/Philippine_Glossy_Starling.jpg',
		'status':'LC',
		'en':{
			'name':'Philippine Glossy Starling ',
			'scientific':'Aplonis panayensis',
			'Class':'Aves',
			'order': 'Passeriformes',
			'distribution':'Assam to Philippines and Sulawesi',
		},
		'tc':{
			'name':'輝椋鳥',
			'scientific':'Aplonis panayensis',
			'Class':'鳥綱',
			'order': '雀形目',
			'distribution':'阿薩姆至菲律賓和蘇拉威西群島',
		},
		'sc':{
			'name':'辉椋鸟',
			'scientific':'Aplonis panayensis',
			'Class':'鸟纲',
			'order': '雀形目',
			'distribution':'阿萨姆至菲律宾和苏拉威西群岛',
		},
	},
	{
		'id':'26',
		'url':'',
		'popup':true,
		'dataID':'bird26',
		'type':'birds',
		'image':'Birds/Blue_and_Yellow_Macaw.jpg',
		'status':'LC',
		'en':{
			'name':'Blue and Yellow Macaw',
			'scientific':'Ara ararauna',
			'Class':'Aves',
			'order': 'Psittaciformes',
			'distribution':'Panama to Paraguay and S. Brazil',
		},
		'tc':{
			'name':'藍黄金鋼鸚鵡',
			'scientific':'Ara ararauna',
			'Class':'鳥綱',
			'order': '鸚形目',
			'distribution':'巴拿馬至巴拉圭和巴西南部',
		},
		'sc':{
			'name':'蓝黄金钢鹦鹉',
			'scientific':'Ara ararauna',
			'Class':'鸟纲',
			'order': '鹦形目',
			'distribution':'巴拿马至巴拉圭和巴西南部',
		},
	},
	{
		'id':'27',
		'url':'animals/birds/animals_32.html',
		'type':'birds',
		'image':'Birds/Hawaiian_Goose.jpg',
		'en':{
			'name':'Hawaiian Goose',
		},
		'tc':{
			'name':'黃頸黑雁',
		},
		'sc':{
			'name':'黄颈黑雁',
		},
	},
	{
		'id':'28',
		'url':'',
		'popup':true,
		'dataID':'bird28',
		'type':'birds',
		'image':'Birds/Yellow_casqued_Hornbill.jpg',
		'status':'VU',
		'en':{
			'name':'Yellow-casqued Hornbill',
			'scientific':'Ceratogymna elata',
			'Class':'Aves',
			'order': 'Bucerotiformes',
			'distribution':'S. Senegal, E. to S.W. Cameroon',
		},
		'tc':{
			'name':'黃盔犀鳥',
			'scientific':'Ceratogymna elata',
			'Class':'鳥綱',
			'order': '犀鳥目',
			'distribution':'非洲塞内加爾南部，喀麥隆東及西南部',
		},
		'sc':{
			'name':'黄盔犀鸟',
			'scientific':'Ceratogymna elata',
			'Class':'鸟纲',
			'order': '犀鸟目',
			'distribution':'非洲塞内加尔南部，喀麦隆东及西南部',
		},
	},
	{
		'id':'59',
		'url':'',
		'popup':true,
		'dataID':'bird59',
		'type':'birds',
		'image':'Birds/Golden_Pheasant.jpg',
		'status':'LC',
		'en':{
			'name':'Golden Pheasant',
			'scientific':'Chrysolophus pictus',
			'Class':'Aves',
			'order': 'Galliformes',
			'distribution':'Central China',
		},
		'tc':{
			'name':'紅腹錦雞',
			'scientific':'Chrysolophus pictus',
			'Class':'鳥綱',
			'order': '雞形目',
			'distribution':'中國中部',
		},
		'sc':{
			'name':'红腹锦鸡',
			'scientific':'Chrysolophus pictus',
			'Class':'鸟纲',
			'order': '鸡形目',
			'distribution':'中国中部',
		},
	},
	{
		'id':'29',
		'url':'',
		'type':'birds',
		'popup':true,
		'dataID':'bird29',
		'image':'Birds/Speckled_pigeon.jpg',
		'status':'LC',
		'en':{
			'name':'Speckled Pigeon',
			'scientific':'Columba guinea',
			'Class':'Aves',
			'order': 'Columbiformes',
			'distribution':'East, South and Central Africa',
		},
		'tc':{
			'name':'點斑鴿',
			'scientific':'Columba guinea',
			'Class':'鳥綱',
			'order': '鴿形目',
			'distribution':'非洲的中、東和南部',
		},
		'sc':{
			'name':'点斑鸽',
			'scientific':'Columba guinea',
			'Class':'鸟纲',
			'order': '鸽形目',
			'distribution':'非洲的中、东和南部',
		},
	},
	{
		'id':'30',
		'url':'',
		'type':'birds',
		'popup':true,
		'dataID':'bird30',
		'image':'Birds/White_browed_Robin_Chat.jpg',
		'status':'LC',
		'en':{
			'name':'White-browed Robin Chat',
			'scientific':'Cossypha heuglini',
			'Class':'Aves',
			'order': 'Passeriformes',
			'distribution':'S. Sudan, Ethiopia to Zambia, Malawi',
		},
		'tc':{
			'name':'白眉歌䳭',
			'scientific':'Cossypha heuglini',
			'Class':'鳥綱',
			'order': '雀形目',
			'distribution':'蘇丹南部，埃塞俄比亞至贊比亞，馬拉維',
		},
		'sc':{
			'name':'白眉歌䳭',
			'scientific':'Cossypha heuglini',
			'Class':'鸟纲',
			'order': '雀形目',
			'distribution':'苏丹南部，埃塞俄比亚至赞比亚，马拉维',
		},
	},
	{
		'id':'31',
		'url':'',
		'type':'birds',
		'popup':true,
		'dataID':'bird31',
		'image':'Birds/Scarlet_Ibis.jpg',
		'status':'LC',
		'en':{
			'name':'Scarlet Ibis',
			'scientific':'Eudocimus ruber',
			'Class':'Aves',
			'order': 'Ciconiiformes',
			'distribution':'Venezuela, Colombia, Guineas, Brazil and Trinidad',
			'video':'https://www.youtube.com/watch?v=eWR0bsbtfx8',
		},
		'tc':{
			'name':'紅䴉',
			'scientific':'Eudocimus ruber',
			'Class':'鳥綱',
			'order': '鸛形目',
			'distribution':'委内瑞拉，哥倫比亞，圭内那，巴西和千里達',
			'video':'https://www.youtube.com/watch?v=scgeQ6QQqQU',
		},
		'sc':{
			'name':'红䴉',
			'scientific':'Eudocimus ruber',
			'Class':'鸟纲',
			'order': '鹳形目',
			'distribution':'委内瑞拉，哥伦比亚，圭内那，巴西和千里达',
			'video':'https://www.youtube.com/watch?v=BAHqxcvEccQ',
		},
	},
	{
		'id':'32',
		'url':'',
		'type':'birds',
		'popup':true,
		'dataID':'bird32',
		'image':'Birds/Black_throated_Laughing_Thrush.jpg',
		'status':'LC',
		'en':{
			'name':'Black-throated Laughing Thrush',
			'scientific':'Garrulax chinensis',
			'Class':'Aves',
			'order': 'Passeriformes',
			'distribution':'Burma to S. China, Hainan',
		},
		'tc':{
			'name':'黑喉噪鶥',
			'scientific':'Garrulax chinensis',
			'Class':'鳥綱',
			'order': '雀形目',
			'distribution':'緬甸至華南，海南島',
		},
		'sc':{
			'name':'黑喉噪鹛',
			'scientific':'Garrulax chinensis',
			'Class':'鸟纲',
			'order': '雀形目',
			'distribution':'缅甸至华南，海南岛',
		},
	},
	{
		'id':'33',
		'url':'',
		'type':'birds',
		'popup':true,
		'dataID':'bird33',
		'image':'Birds/Blue_crowned_Pigeon.jpg',
		'status':'VU',
		'en':{
			'name':'Blue-crowned Pigeon',
			'scientific':'Goura cristata',
			'Class':'Aves',
			'order': 'Columbiformes',
			'distribution':'N.W. New Guinea and W. Papuan Islands',
		},
		'tc':{
			'name':'藍鳳冠鳩',
			'scientific':'Goura cristata',
			'Class':'鳥綱',
			'order': '鴿形目',
			'distribution':'新幾内亞西北部和巴布亞群島西部',
		},
		'sc':{
			'name':'蓝凤冠鸠',
			'scientific':'Goura cristata',
			'Class':'鸟纲',
			'order': '鸽形目',
			'distribution':'新几内亚西北部和巴布亚群岛西部',
		},
	},
	{
		'id':'34',
		'url':'',
		'type':'birds',
		'popup':true,
		'dataID':'bird34',
		'status':'VU',
		'image':'Birds/Maroon_breasted_Crowned_Pigeon.jpg',
		'en':{
			'name':'Maroon-breasted Crowned Pigeon',
			'scientific':'Goura scheepmakeri',
			'Class':'Aves',
			'order': 'Columbiformes',
			'distribution':'S. and S.E. New Guinea',
		},
		'tc':{
			'name':'紫胸鳳冠鳩',
			'scientific':'Goura scheepmakeri',
			'Class':'鳥綱',
			'order': '鴿形目',
			'distribution':'新幾内亞南部和東南部',
		},
		'sc':{
			'name':'紫胸凤冠鸠',
			'scientific':'Goura scheepmakeri',
			'Class':'鸟纲',
			'order': '鸽形目',
			'distribution':'新几内亚南部和东南部',
		},
	},
	{
		'id':'35',
		'url':'animals/birds/animals_33.html',
		'type':'birds',
		'image':'Birds/Red_crowned_Crane.jpg',
		'en':{
			'name':'Red-crowned Crane',
		},
		'tc':{
			'name':'丹頂鶴',
		},
		'sc':{
			'name':'丹顶鹤',
		},
	},
	{
		'id':'36',
		'url':'',
		'type':'birds',
		'popup':true,
		'dataID':'bird36',
		'image':'Birds/Emerald_Starling.jpg',
		'status':'LC',
		'en':{
			'name':'Emerald Starling',
			'scientific':'Lamprotornis iris',
			'Class':'Aves',
			'order': 'Passeriformes',
			'distribution':'West Africa',
		},
		'tc':{
			'name':'翠綠椋鳥',
			'scientific':'Lamprotornis iris',
			'Class':'鳥綱',
			'order': '雀形目',
			'distribution':'非洲西部',
		},
		'sc':{
			'name':'翠绿椋鸟',
			'scientific':'Lamprotornis iris',
			'Class':'鸟纲',
			'order': '雀形目',
			'distribution':'非洲西部',
		},
	},
	{
		'id':'37',
		'url':'animals/birds/animals_31.html',
		'type':'birds',
		'image':'Birds/Bali_Myna.jpg',
		'en':{
			'name':'Bali Myna',
		},
		'tc':{
			'name':'長冠八哥',
		},
		'sc':{
			'name':'长冠八哥',
		},
	},
	{
		'id':'38',
		'url':'',
		'type':'birds',
		'popup':true,
		'dataID':'bird38',
		'status':'LC',
		'image':'Birds/Silver_Pheasant.jpg',
		'en':{
			'name':'Silver Pheasant',
			'scientific':'Lophura nycthemera',
			'Class':'Aves',
			'order': 'Galliformes',
			'distribution':'S. China, Hainan to S.E. Asia',
		},
		'tc':{
			'name':'白鷴',
			'scientific':'Lophura nycthemera',
			'Class':'鳥綱',
			'order': '雞形目',
			'distribution':'華南，海南島至東南亞',
		},
		'sc':{
			'name':'白鹇',
			'scientific':'Lophura nycthemera',
			'Class':'鸟纲',
			'order': '鸡形目',
			'distribution':'华南，海南岛至东南亚',
		},
	},
	{
		'id':'39',
		'url':'animals/birds/animals_34.html',
		'type':'birds',
		'image':'Birds/American_Flamingo.jpg',
		'en':{
			'name':'American Flamingo',
		},
		'tc':{
			'name':'美洲紅鸛',
		},
		'sc':{
			'name':'美洲红鹳',
		},
	},
	{
		'id':'40',
		'url':'',
		'type':'birds',
		'popup':true,
		'dataID':'bird40',
		'image':'Birds/Malay_Peacock_Pheasant.jpg',
		'status':'LC',
		'en':{
			'name':'Malay Peacock Pheasant',
			'scientific':'Polyplectron malacense',
			'Class':'Aves',
			'order': 'Galliformes',
			'distribution':'S.W. Thailand, Malay Peninsula',
		},
		'tc':{
			'name':'馬來孔雀雉',
			'scientific':'Polyplectron malacense',
			'Class':'鳥綱',
			'order': '雞形目',
			'distribution':'泰國西南部，馬來半島',
		},
		'sc':{
			'name':'马来孔雀雉',
			'scientific':'Polyplectron malacense',
			'Class':'鸟纲',
			'order': '鸡形目',
			'distribution':'泰国西南部，马来半岛',
		},
	},
	{
		'id':'41',
		'url':'',
		'type':'birds',
		'popup':true,
		'dataID':'bird41',
		'image':'Birds/Common_Shelduck.jpg',
		'status':'LC',
		'en':{
			'name':'Common Shelduck',
			'scientific':'Tadorna tadorna',
			'Class':'Aves',
			'order': 'Anseriformes',
			'distribution':'Breed W. Europe to E. Asia, Winters N. Africa, India & S. China (including Hong Kong)',
		},
		'tc':{
			'name':'翹鼻麻鴨',
			'scientific':'Tadorna tadorna',
			'Class':'鳥綱',
			'order': '雁形目',
			'distribution':'在西歐至東亞繁殖而在北非，印度和華南過冬（包括香港）',
		},
		'sc':{
			'name':'翘鼻麻鸭',
			'scientific':'Tadorna tadorna',
			'Class':'鸟纲',
			'order': '雁形目',
			'distribution':'在西欧至东亚繁殖而在北非，印度和华南过冬（包括香港）',
		},
	},
	{
		'id':'42',
		'url':'',
		'type':'birds',
		'popup':true,
		'dataID':'bird42',
		'image':'Birds/Sacred_Ibis.jpg',
		'status':'LC',
		'en':{
			'name':'Sacred Ibis',
			'scientific':'Threskiornis aethiopicus',
			'Class':'Aves',
			'order': 'Ciconiiformes',
			'distribution':'Africa, W. Asia, S.W. Pacific, Australasia',
		},
		'tc':{
			'name':'聖䴉',
			'scientific':'Threskiornis aethiopicus',
			'Class':'鳥綱',
			'order': '鸛形目',
			'distribution':'非洲至亞洲西部，太平洋西南部和澳大拉西亞',
		},
		'sc':{
			'name':'圣䴉',
			'scientific':'Threskiornis aethiopicus',
			'Class':'鸟纲',
			'order': '鹳形目',
			'distribution':'非洲至亚洲西部，太平洋西南部和澳大拉西亚',
		},
	},
	{
		'id':'43',
		'url':'',
		'type':'birds',
		'popup':true,
		'dataID':'bird43',
		'image':'Birds/Red_billed_Hornbill.jpg',
		'status':'LC',
		'en':{
			'name':'Red-billed Hornbill',
			'scientific':'Tockus erythrorhynchus',
			'Class':'Aves',
			'order': 'Bucerotiformes',
			'distribution':'Inner E. Niger Delta to Ethiopia, S. Somalia to Tanzania',
		},
		'tc':{
			'name':'紅喙彎嘴犀鳥',
			'scientific':'Tockus erythrorhynchus',
			'Class':'鳥綱',
			'order': '犀鳥目',
			'distribution':'内尼日爾三角洲東部至埃塞俄比亞，索馬里南部至坦桑尼亞',
		},
		'sc':{
			'name':'红喙弯嘴犀鸟',
			'scientific':'Tockus erythrorhynchus',
			'Class':'鸟纲',
			'order': '犀鸟目',
			'distribution':'内尼日尔三角洲东部至埃塞俄比亚，索马里南部至坦桑尼亚',
		},
	},
	{
		'id':'44',
		'url':'',
		'type':'birds',
		'popup':true,
		'dataID':'bird44',
		'image':'Birds/Oriental_Pied_Hornbill.jpg',
		'status':'LC',
		'en':{
			'name':'Oriental Pied Hornbill',
			'scientific':'Anthracoceros albirostris',
			'Class':'Aves',
			'order': 'Bucerotiformes',
			'distribution':'China, Southeast Asia',
		},
		'tc':{
			'name':'冠斑犀鳥',
			'scientific':'Anthracoceros albirostriss',
			'Class':'鳥綱',
			'order': '犀鳥目',
			'distribution':'中國及東南亞',
		},
		'sc':{
			'name':'冠斑犀鸟',
			'scientific':'Anthracoceros albirostriss',
			'Class':'鸟纲',
			'order': '犀鸟目',
			'distribution':'中国及东南亚',
		},
	},
	{
		'id':'45',
		'url':'',
		'type':'birds',
		'popup':true,
		'dataID':'bird45',
		'image':'Birds/Old_German_Owl_Black_Bar_Shield.jpg',
		'status':'LC',
		'en':{
			'name':'Old German Owl-Black Bar Shield',
			'scientific':'Columba livia',
			'Class':'Aves',
			'order': 'Columbiformes',
			'distribution':'Germany',
		},
		'tc':{
			'name':'黑披肩鴿',
			'scientific':'Columba livia',
			'Class':'鳥綱',
			'order': '鴿形目',
			'distribution':'德國',
		},
		'sc':{
			'name':'黑披肩鸽',
			'scientific':'Columba livia',
			'Class':'鸟纲',
			'order': '鸽形目',
			'distribution':'德国',
		},
	},
	{
		'id':'46',
		'url':'',
		'type':'birds',
		'popup':true,
		'dataID':'bird46',
		'image':'Birds/Old_German_Owl_Shield_Mark_Silver_Bar.jpg',
		'status':'LC',
		'en':{
			'name':'Old German Owl- Shield Mark (Silver Bar)',
			'scientific':'Columba livia',
			'Class':'Aves',
			'order': 'Columbiformes',
			'distribution':'Germany',
		},
		'tc':{
			'name':'鷹嘴鴿',
			'scientific':'Columba livia',
			'Class':'鳥綱',
			'order': '鴿形目',
			'distribution':'德國',
		},
		'sc':{
			'name':'鹰嘴鸽',
			'scientific':'Columba livia',
			'Class':'鸟纲',
			'order': '鸽形目',
			'distribution':'德国',
		},
	},
	{
		'id':'47',
		'url':'',
		'type':'birds',
		'popup':true,
		'dataID':'bird47',
		'image':'Birds/Old_German_Owl_Fan_Tail.jpg',
		'status':'LC',
		'en':{
			'name':'Old German Owl-Fan Tail',
			'scientific':'Columba livia',
			'Class':'Aves',
			'order': 'Columbiformes',
			'distribution':'Germany',
		},
		'tc':{
			'name':'扇尾鴿',
			'scientific':'Columba livia',
			'Class':'鳥綱',
			'order': '鴿形目',
			'distribution':'德國',
		},
		'sc':{
			'name':'扇尾鸽',
			'scientific':'Columba livia',
			'Class':'鸟纲',
			'order': '鸽形目',
			'distribution':'德国',
		},
	},
	{
		'id':'48',
		'url':'',
		'type':'birds',
		'popup':true,
		'dataID':'bird48',
		'image':'Birds/Old_German_Owl_Brown_Bar_Shield.jpg',
		'status':'LC',
		'en':{
			'name':'Old German Owl-Brown Bar Shield',
			'scientific':'Columba livia',
			'Class':'Aves',
			'order': 'Columbiformes',
			'distribution':'Germany',
		},
		'tc':{
			'name':'啡披肩鴿',
			'scientific':'Columba livia',
			'Class':'鳥綱',
			'order': '鴿形目',
			'distribution':'德國',
		},
		'sc':{
			'name':'啡披肩鸽',
			'scientific':'Columba livia',
			'Class':'鸟纲',
			'order': '鸽形目',
			'distribution':'德国',
		},
	},
	{
		'id':'49',
		'url':'',
		'type':'birds',
		'popup':true,
		'dataID':'bird49',
		'image':'Birds/Laughing_Kookaburra.jpg',
		'status':'LC',
		'en':{
			'name':'Laughing Kookaburra',
			'scientific':'Dacelo novaeguineae',
			'Class':'Aves',
			'order': 'Coraciiformes',
			'distribution':'Australia',
		},
		'tc':{
			'name':'笑翠鳥',
			'scientific':'Dacelo novaeguineae',
			'Class':'鳥綱',
			'order': '佛法僧目',
			'distribution':'澳洲',
		},
		'sc':{
			'name':'笑翠鸟',
			'scientific':'Dacelo novaeguineae',
			'Class':'鸟纲',
			'order': '佛法僧目',
			'distribution':'澳洲',
		},
	},
	{
		'id':'50',
		'url':'animals/birds/animals_36.html',
		'type':'birds',
		'image':'Birds/Black_faced_Spoonbill.jpg',
		'en':{
			'name':'Black-faced Spoonbill',
		},
		'tc':{
			'name':'黑臉琵鷺',
		},
		'sc':{
			'name':'黑脸琵鹭',
		},
	},
	{
		'id':'51',
		'url':'',
		'type':'birds',
		'popup':true,
		'dataID':'bird51',
		'image':'Birds/Red_sided_Eclectus_Parrot.jpg',
		'status':'LC',
		'en':{
			'name':'Red-sided Eclectus Parrot',
			'scientific':'Eclectus roratus polychloros',
			'Class':'Aves',
			'order': 'Psittaciformes',
			'distribution':'Indonesia, New Guinea, Australia',
		},
		'tc':{
			'name':'紅翅折衷鸚鵡',
			'scientific':'Eclectus roratus polychloros',
			'Class':'鳥綱',
			'order': '鸚形目',
			'distribution':'印度尼西亞、新幾内亞、澳洲',
		},
		'sc':{
			'name':'红翅折衷鹦鹉',
			'scientific':'Eclectus roratus polychloros',
			'Class':'鸟纲',
			'order': '鹦形目',
			'distribution':'印度尼西亚、新几内亚、澳洲',
		},
	},
	{
		'id':'52',
		'url':'',
		'type':'birds',
		'popup':true,
		'dataID':'bird52',
		'image':'Birds/Nicobar_Pigeon.jpg',
		'status':'NT',
		'en':{
			'name':'Nicobar Pigeon',
			'scientific':'Caloenas nicobarica',
			'Class':'Aves',
			'order': 'Columbiformes',
			'distribution':'Woodlands or forests on small islands in the Indo-Pacific region, including countries such as the Philippines, Indonesia and Papua New Guinea',
		},
		'tc':{
			'name':'尼柯巴鳩',
			'scientific':'Caloenas nicobarica',
			'Class':'鳥綱',
			'order': '鴿形目',
			'distribution':'印度太平洋小島上的森林或林地，包括菲律賓、印尼和巴布亞新幾內亞等地',
		},
		'sc':{
			'name':'尼柯巴鸠',
			'scientific':'Caloenas nicobarica',
			'Class':'鸟纲',
			'order': '鸽形目',
			'distribution':'印度太平洋小岛上的森林或林地，包括菲律宾、印尼和巴布亚新几内亚等地',
		},
	},
	{
		'id':'53',
		'url':'animals/birds/animals_65.html',
		'type':'birds',
		'image':'Birds/Black_Crowned_Crane.jpg',
		'en':{
			'name':'Black Crowned Crane',
		},
		'tc':{
			'name':'黑冕鶴',
		},
		'sc':{
			'name':'黑冕鹤',
		},
	},
	{
		'id':'54',
		'url':'',
		'type':'birds',
		'popup':true,
		'dataID':'bird54',
		'image':'Birds/Toco_Toucan.jpg',
		'status':'LC',
		'en':{
			'name':'Toco Toucan',
			'scientific':'Ramphastos toco',
			'Class':'Aves',
			'order': 'Piciformes',
			'distribution':'Bolivia, Peru, Argentina, Paraguay, and Brazil',
		},
		'tc':{
			'name':'鞭笞巨嘴鳥',
			'scientific':'Ramphastos toco',
			'Class':'鳥綱',
			'order': '鴷形目',
			'distribution':'玻利維亞、秘魯、阿根廷、巴拉圭、及巴西',
		},
		'sc':{
			'name':'鞭笞巨嘴鸟',
			'scientific':'Ramphastos toco',
			'Class':'鸟纲',
			'order': '鴷形目',
			'distribution':'玻利维亚、秘鲁、阿根廷、巴拉圭、及巴西',
		},
	},
	{
		'id':'55',
		'url':'',
		'type':'birds',
		'popup':true,
		'dataID':'bird55',
		'image':'Birds/Red_billed_Toucan.jpg',
		'status':'VU',
		'en':{
			'name':'Red-billed Toucan',
			'scientific':'Ramphastos tucanus',
			'Class':'Aves',
			'order': 'Piciformes',
			'distribution':'Colombia, Ecuador, Peru, Bolivia, Venezuela, and Brazil',
		},
		'tc':{
			'name':'紅嘴巨嘴鳥',
			'scientific':'Ramphastos tucanus',
			'Class':'鳥綱',
			'order': '鴷形目',
			'distribution':'哥倫比亞、厄瓜多、秘魯、玻利維亞、委內瑞拉、及巴西',
		},
		'sc':{
			'name':'红嘴巨嘴鸟',
			'scientific':'Ramphastos tucanus',
			'Class':'鸟纲',
			'order': '鴷形目',
			'distribution':'哥伦比亚、厄瓜多、秘鲁、玻利维亚、委内瑞拉、及巴西',
		},
	},
	{
		'id':'56',
		'url':'',
		'type':'birds',
		'popup':true,
		'dataID':'bird56',
		'image':'Birds/Torre_Strait_Pigeon.jpg',
		'status':'LC',
		'en':{
			'name':"Torre’s Strait Pigeon",
			'scientific':'Ducula spilorrhoa',
			'Class':'Aves',
			'order': 'Columbiformes',
			'distribution':'Northern Australia and New Guinea',
		},
		'tc':{
			'name':'澳洲斑皇鳩',
			'scientific':'Ducula spilorrhoa',
			'Class':'鳥綱',
			'order': '鴿形目',
			'distribution':'澳洲北部和新幾内亞一帶海岸',
		},
		'sc':{
			'name':'澳洲斑皇鸠',
			'scientific':'Ducula spilorrhoa',
			'Class':'鸟纲',
			'order': '鸽形目',
			'distribution':'澳洲北部和新几内亚一带海岸',
		},
	},
	// {
	// 	'id':'57',
	// 	'url':'',
	// 	'type':'birds',
	// 	'popup':true,
	// 	'dataID':'bird57',
	// 	'image':'Birds/Great_White_Pelican.jpg',
	// 	'status':'LC',
	// 	'en':{
	// 		'name':'Great White Pelican',
	// 		'scientific':'Pelecanus onocrotalus',
	// 		'Class':'Aves',
	// 		'order': 'Pelecaniformes',
	// 		'distribution':'Europe, Asia and Africa',
	// 	},
	// 	'tc':{
	// 		'name':'白鵜鶘',
	// 		'scientific':'Pelecanus onocrotalus',
	// 		'Class':'鳥綱',
	// 		'order': '鵜形目',
	// 		'distribution':'歐洲、亞洲及非洲',
	// 	},
	// 	'sc':{
	// 		'name':'白鹈鹕',
	// 		'scientific':'Pelecanus onocrotalus',
	// 		'Class':'鸟纲',
	// 		'order': '鹈形目',
	// 		'distribution':'欧洲、亚洲及非洲',
	// 	},
	// },
	{
		'id':'58',
		'url':'',
		'type':'birds',
		'popup':true,
		'dataID':'bird58',
		'image':'Birds/Great_Curassow.jpg',
		'status':'VU',
		'en':{
			'name':'Great Curassow',
			'scientific':'Crax rubra',
			'Class':'Aves',
			'order': 'Galliformes',
			'distribution':'Colombia, Costa Rica, Guatemala, Honduras',
		},
		'tc':{
			'name':'大鳳冠雉',
			'scientific':'Crax rubra',
			'Class':'鳥綱',
			'order': '雞形目',
			'distribution':'哥倫比亞, 哥斯達黎加、危地馬拉, 洪都拉斯',
		},
		'sc':{
			'name':'大凤冠雉',
			'scientific':'Crax rubra',
			'Class':'鸟纲',
			'order': '鸡形目',
			'distribution':'哥伦比亚, 哥斯达黎加、危地马拉, 洪都拉斯',
		},
	},
];

var map_data = [
	{
		'id': '1',
		'image': 'mammals.png',
		'filters': 'mammals',
		'top': '28.33', 
		'left': '30',
	},
	{
		'id': '2',
		'image': 'mammals.png',
		'filters': 'mammals',
		'top': '24', 
		'left': '36',
	},
	{
		'id': '3',
		'image': 'mammals.png',
		'filters': 'mammals',
		'top': '25', 
		'left': '41',
	},
	{
		'id': '4',
		'image': 'mammals.png',
		'filters': 'mammals',
		'top': '29', 
		'left': '46',
	},
	{
		'id': '5',
		'image': 'mammals.png',
		'filters': 'mammals',
		'top': '16', 
		'left': '53',
	},
	{
		'id': '6',
		'image': 'mammals.png',
		'filters': 'mammals',
		'top': '57', 
		'left': '45',
	},
	{
		'id': '7',
		'image': 'mammals.png',
		'filters': 'mammals',
		'top': '56', 
		'left': '48.5',
	},
	{
		'id': '8',
		'image': 'birds.png',
		'filters': 'birds',
		'top': '63', 
		'left': '52.5',
	},
	{
		'id': '9',
		'image': 'birds.png',
		'filters': 'birds',
		'top': '54', 
		'left': '50.5',
	},
	{
		'id': '10',
		'image': 'birds.png',
		'filters': 'birds',
		'top': '60', 
		'left': '57.5',
	},
	{
		'id': '11',
		'image': 'birds.png',
		'filters': 'birds',
		'top': '54', 
		'left': '62.5',
	},
	{
		'id': '12',
		'image': 'reptiles.png',
		'filters': 'reptiles',
		'top': '29', 
		'left': '40',
	},
	{
		'id': '13',
		'image': 'reptiles.png',
		'filters': 'reptiles',
		'top': '31', 
		'left': '43.5',
	},
	{
		'id': '14',
		'image': 'toilet.png',
		'filters': 'toilet',
		'top': '47', 
		'left': '42.5',
	},
	{
		'id': '15',
		'image': 'toilet.png',
		'filters': 'toilet',
		'top': '72', 
		'left': '38.5',
	},
	{
		'id': '16',
		'image': 'toilet.png',
		'filters': 'toilet',
		'top': '57', 
		'left': '74',
	},
	{
		'id': '14',
		'image': 'disabled_toilet.png',
		'filters': 'disabled_toilet',
		'top': '49', 
		'left': '44',
	},
	{
		'id': '15',
		'image': 'disabled_toilet.png',
		'filters': 'disabled_toilet',
		'top': '75', 
		'left': '39.5',
	},
	{
		'id': '16',
		'image': 'disabled_toilet.png',
		'filters': 'disabled_toilet',
		'top': '57', 
		'left': '76.5',
	},
	{
		'id': '17',
		'image': 'playground.png',
		'filters': 'playground',
		'top': '70.33', 
		'left': '43',
	},
	{
		'id': '18',
		'image': 'kiosk.png',
		'filters': 'kiosk',
		'top': '28.1', 
		'left': '57',
	},
	{
		'id': '19',
		'image': 'office.png',
		'filters': 'office',
		'top': '26.1', 
		'left': '58.33',
	},
	{
		'id': '20',
		'image': 'centre.png',
		'filters': 'centre',
		'top': '37.1', 
		'left': '46.33',
	},
	{
		'id': '21',
		'image': 'shelter.png',
		'filters': 'shelter',
		'top': '33.5', 
		'left': '40.8',
	},
	{
		'id': '22',
		'image': 'shelter.png',
		'filters': 'shelter',
		'top': '49.1', 
		'left': '65.33',
	},
	{
		'id': '23',
		'image': 'parking.png',
		'filters': 'parking',
		'top': '84', 
		'left': '65.5',
	},
	{
		'id': '24',
		'image': 'visitor.png',
		'filters': 'visitor',
		'top': '32.4', 
		'left': '56.9',
	},
	{
		'id': '25',
		'image': 'fountain.png',
		'filters': 'fountain',
		'top': '39.5', 
		'left': '64.5',
	},
	{
		'id': '26',
		'image': 'flight.png',
		'filters': 'flight',
		'top': '48', 
		'left': '74.5',
	},
	{
		'id': '27',
		'image': 'memorial.png',
		'filters': 'memorial',
		'top': '48', 
		'left': '82',
	},
	{
		'id': '28',
		'image': 'pavilion.png',
		'filters': 'pavilion',
		'top': '45', 
		'left': '55',
	},
	{
		'id': '29',
		'image': 'bronze.png',
		'filters': 'bronze',
		'top': '54', 
		'left': '55',
	},
	{
		'id': '30',
		'image': 'stone.png',
		'filters': 'stone',
		'top': '44', 
		'left': '83.5',
	},
	{
		'id': '31',
		'image': 'gate.png',
		'filters': 'gate',
		'top': '77.65', 
		'left': '52',
	},
	{
		'id': '32',
		'image': 'greenHouse.png',
		'filters': 'greenHouse',
		'top': '73', 
		'left': '58',
	},
	{
		'id': '33',
		'image': 'thematic.png',
		'filters': 'thematic',
		'top': '20.5', 
		'left': '37',
	},
	{
		'id': '34',
		'image': 'thematic.png',
		'filters': 'thematic',
		'top': '39', 
		'left': '39.3',
	},
	{
		'id': '35',
		'image': 'thematic.png',
		'filters': 'thematic',
		'top': '46.8', 
		'left': '51',
	},
	{
		'id': '36',
		'image': 'thematic.png',
		'filters': 'thematic',
		'top': '59.6', 
		'left': '41',
	},
	{
		'id': '37',
		'image': 'thematic.png',
		'filters': 'thematic',
		'top': '51', 
		'left': '55',
	},
	{
		'id': '38',
		'image': 'thematic.png',
		'filters': 'thematic',
		'top': '48', 
		'left': '61',
	},
	{
		'id': '39',
		'image': 'thematic.png',
		'filters': 'thematic',
		'top': '44', 
		'left': '73.4',
	},
	{
		'id': '40',
		'image': 'thematic.png',
		'filters': 'thematic',
		'top': '46', 
		'left': '77',
	},
	{
		'id': '41',
		'image': 'thematic.png',
		'filters': 'thematic',
		'top': '50', 
		'left': '79',
	},
	{
		'id': '42',
		'image': 'thematic.png',
		'filters': 'thematic',
		'top': '58.3', 
		'left': '69.5',
	},
	{
		'id': '43',
		'image': 'ovts.png',
		'filters': 'ovts',
		'top': '50', 
		'left': '50',
	},
	{
		'id': '44',
		'image': 'heritage.png',
		'filters': 'heritage',
		'top': '50', 
		'left': '50',
	},
];
var month_data = [ 
	{
		'id':'1',
		'url':'../plants/month/jul16.html',
		'year':'2016',
		'image':'month/jul16.jpg',
		'en':{
			'month':"July",
		},
		'tc':{
			'month':'七月份',
		},
		'sc':{
			'month':'七月份',
		},
	},
	{
		'id':'2',
		'url':'../plants/month/aug16.html',
		'year':'2016',
		'image':'month/aug16.jpg',
		'en':{
			'month':"August",
		},
		'tc':{
			'month':'八月份',
		},
		'sc':{
			'month':'八月份',
		},
	},
	{
		'id':'3',
		'url':'../plants/month/sep16.html',
		'year':'2016',
		'image':'month/sep16.jpg',
		'en':{
			'month':"September",
		},
		'tc':{
			'month':'九月份',
		},
		'sc':{
			'month':'九月份',
		},
	},
	{
		'id':'4',
		'url':'../plants/month/oct16.html',
		'year':'2016',
		'image':'month/oct16.jpg',
		'en':{
			'month':"October",
		},
		'tc':{
			'month':'十月份',
		},
		'sc':{
			'month':'十月份',
		},
	},
	{
		'id':'5',
		'url':'../plants/month/nov16.html',
		'year':'2016',
		'image':'month/nov16.jpg',
		'en':{
			'month':"November",
		},
		'tc':{
			'month':'十一月份',
		},
		'sc':{
			'month':'十一月份',
		},
	},
	{
		'id':'6',
		'url':'../plants/month/dec16.html',
		'year':'2016',
		'image':'month/dec16.jpg',
		'en':{
			'month':"December",
		},
		'tc':{
			'month':'十二月份',
		},
		'sc':{
			'month':'十二月份',
		},
	},
	{
		'id':'7',
		'url':'../plants/month/jan17.html',
		'year':'2017',
		'image':'month/jan17.jpg',
		'en':{
			'month':"January",
		},
		'tc':{
			'month':'一月份',
		},
		'sc':{
			'month':'一月份',
		},
	},
	{
		'id':'8',
		'url':'../plants/month/feb17.html',
		'year':'2017',
		'image':'month/feb17.jpg',
		'en':{
			'month':"February",
		},
		'tc':{
			'month':'二月份',
		},
		'sc':{
			'month':'二月份',
		},
	},
	{
		'id':'9',
		'url':'../plants/month/mar17.html',
		'year':'2017',
		'image':'month/mar17.jpg',
		'en':{
			'month':"March",
		},
		'tc':{
			'month':'三月份',
		},
		'sc':{
			'month':'三月份',
		},
	},
	{
		'id':'10',
		'url':'../plants/month/apr17.html',
		'year':'2017',
		'image':'month/apr17.jpg',
		'en':{
			'month':"April",
		},
		'tc':{
			'month':'四月份',
		},
		'sc':{
			'month':'四月份',
		},
	},
	{
		'id':'11',
		'url':'../plants/month/may17.html',
		'year':'2017',
		'image':'month/may17.jpg',
		'en':{
			'month':"May",
		},
		'tc':{
			'month':'五月份',
		},
		'sc':{
			'month':'五月份',
		},
	},
	{
		'id':'12',
		'url':'../plants/month/jun17.html',
		'year':'2017',
		'image':'month/jun17.jpg',
		'en':{
			'month':"June",
		},
		'tc':{
			'month':'六月份',
		},
		'sc':{
			'month':'六月份',
		},
	},
	{
		'id':'13',
		'url':'../plants/month/jul17.html',
		'year':'2017',
		'image':'month/jul17.jpg',
		'en':{
			'month':"July",
		},
		'tc':{
			'month':'七月份',
		},
		'sc':{
			'month':'七月份',
		},
	},
	{
		'id':'14',
		'url':'../plants/month/aug17.html',
		'year':'2017',
		'image':'month/aug17.jpg',
		'en':{
			'month':"August",
		},
		'tc':{
			'month':'八月份',
		},
		'sc':{
			'month':'八月份',
		},
	},
	{
		'id':'15',
		'url':'../plants/month/sep17.html',
		'year':'2017',
		'image':'month/sep17.jpg',
		'en':{
			'month':"September",
		},
		'tc':{
			'month':'九月份',
		},
		'sc':{
			'month':'九月份',
		},
	},
	{
		'id':'16',
		'url':'../plants/month/oct17.html',
		'year':'2017',
		'image':'month/oct17.jpg',
		'en':{
			'month':"October",
		},
		'tc':{
			'month':'十月份',
		},
		'sc':{
			'month':'十月份',
		},
	},
	{
		'id':'17',
		'url':'../plants/month/nov17.html',
		'year':'2017',
		'image':'month/nov17.jpg',
		'en':{
			'month':"November",
		},
		'tc':{
			'month':'十一月份',
		},
		'sc':{
			'month':'十一月份',
		},
	},
	{
		'id':'18',
		'url':'../plants/month/dec17.html',
		'year':'2017',
		'image':'month/dec17.jpg',
		'en':{
			'month':"December",
		},
		'tc':{
			'month':'十二月份',
		},
		'sc':{
			'month':'十二月份',
		},
	},
	{
		'id':'19',
		'url':'../plants/month/jan18.html',
		'year':'2018',
		'image':'month/jan18.jpg',
		'en':{
			'month':"January",
		},
		'tc':{
			'month':'一月份',
		},
		'sc':{
			'month':'一月份',
		},
	},
	{
		'id':'29',
		'url':'../plants/month/feb18.html',
		'year':'2018',
		'image':'month/feb18.jpg',
		'en':{
			'month':"February",
		},
		'tc':{
			'month':'二月份',
		},
		'sc':{
			'month':'二月份',
		},
	},
	{
		'id':'21',
		'url':'../plants/month/mar18.html',
		'year':'2018',
		'image':'month/mar18.jpg',
		'en':{
			'month':"March",
		},
		'tc':{
			'month':'三月份',
		},
		'sc':{
			'month':'三月份',
		},
	},
	{
		'id':'22',
		'url':'../plants/month/apr18.html',
		'year':'2018',
		'image':'month/apr18.jpg',
		'en':{
			'month':"April",
		},
		'tc':{
			'month':'四月份',
		},
		'sc':{
			'month':'四月份',
		},
	},
	{
		'id':'23',
		'url':'../plants/month/may18.html',
		'year':'2018',
		'image':'month/may18.jpg',
		'en':{
			'month':"May",
		},
		'tc':{
			'month':'五月份',
		},
		'sc':{
			'month':'五月份',
		},
	},
	{
		'id':'24',
		'url':'../plants/month/jun18.html',
		'year':'2018',
		'image':'month/jun18.jpg',
		'en':{
			'month':"June",
		},
		'tc':{
			'month':'六月份',
		},
		'sc':{
			'month':'六月份',
		},
	},
	{
		'id':'25',
		'url':'../plants/month/jul18.html',
		'year':'2018',
		'image':'month/jul18.jpg',
		'en':{
			'month':"July",
		},
		'tc':{
			'month':'七月份',
		},
		'sc':{
			'month':'七月份',
		},
	},
	{
		'id':'26',
		'url':'../plants/month/aug18.html',
		'year':'2018',
		'image':'month/aug18.jpg',
		'en':{
			'month':"August",
		},
		'tc':{
			'month':'八月份',
		},
		'sc':{
			'month':'八月份',
		},
	},
	{
		'id':'27',
		'url':'../plants/month/sep18.html',
		'year':'2018',
		'image':'month/sep18.jpg',
		'en':{
			'month':"September",
		},
		'tc':{
			'month':'九月份',
		},
		'sc':{
			'month':'九月份',
		},
	},
	{
		'id':'28',
		'url':'../plants/month/oct18.html',
		'year':'2018',
		'image':'month/oct18.jpg',
		'en':{
			'month':"October",
		},
		'tc':{
			'month':'十月份',
		},
		'sc':{
			'month':'十月份',
		},
	},
	{
		'id':'29',
		'url':'../plants/month/nov18.html',
		'year':'2018',
		'image':'month/nov18.jpg',
		'en':{
			'month':"November",
		},
		'tc':{
			'month':'十一月份',
		},
		'sc':{
			'month':'十一月份',
		},
	},
	{
		'id':'30',
		'url':'../plants/month/dec18.html',
		'year':'2018',
		'image':'month/dec18.jpg',
		'en':{
			'month':"December",
		},
		'tc':{
			'month':'十二月份',
		},
		'sc':{
			'month':'十二月份',
		},
	},
	{
		'id':'31',
		'url':'../plants/month/jan19.html',
		'year':'2019',
		'image':'month/jan.jpg',
		'en':{
			'month':"January",
		},
		'tc':{
			'month':'一月份',
		},
		'sc':{
			'month':'一月份',
		},
	},
	{
		'id':'32',
		'url':'../plants/month/feb19.html',
		'year':'2019',
		'image':'month/feb.jpg',
		'en':{
			'month':"February",
		},
		'tc':{
			'month':'二月份',
		},
		'sc':{
			'month':'二月份',
		},
	},
	{
		'id':'33',
		'url':'../plants/month/mar19.html',
		'year':'2019',
		'image':'month/mar.jpg',
		'en':{
			'month':"March",
		},
		'tc':{
			'month':'三月份',
		},
		'sc':{
			'month':'三月份',
		},
	},
	{
		'id':'34',
		'url':'../plants/month/apr19.html',
		'year':'2019',
		'image':'month/apr.jpg',
		'en':{
			'month':"April",
		},
		'tc':{
			'month':'四月份',
		},
		'sc':{
			'month':'四月份',
		},
	},
	{
		'id':'35',
		'url':'../plants/month/may19.html',
		'year':'2019',
		'image':'month/may.jpg',
		'en':{
			'month':"May",
		},
		'tc':{
			'month':'五月份',
		},
		'sc':{
			'month':'五月份',
		},
	},
	{
		'id':'36',
		'url':'../plants/month/jun19.html',
		'year':'2019',
		'image':'month/jun.jpg',
		'en':{
			'month':"June",
		},
		'tc':{
			'month':'六月份',
		},
		'sc':{
			'month':'六月份',
		},
	},
	{
		'id':'37',
		'url':'../plants/month/jul19.html',
		'year':'2019',
		'image':'month/jul.jpg',
		'en':{
			'month':"July",
		},
		'tc':{
			'month':'七月份',
		},
		'sc':{
			'month':'七月份',
		},
	},
	{
		'id':'38',
		'url':'../plants/month/aug19.html',
		'year':'2019',
		'image':'month/aug.jpg',
		'en':{
			'month':"August",
		},
		'tc':{
			'month':'八月份',
		},
		'sc':{
			'month':'八月份',
		},
	},
	{
		'id':'39',
		'url':'../plants/month/sep19.html',
		'year':'2019',
		'image':'month/sep.jpg',
		'en':{
			'month':"September",
		},
		'tc':{
			'month':'九月份',
		},
		'sc':{
			'month':'九月份',
		},
	},
	{
		'id':'40',
		'url':'../plants/month/oct19.html',
		'year':'2019',
		'image':'month/oct.jpg',
		'en':{
			'month':"October",
		},
		'tc':{
			'month':'十月份',
		},
		'sc':{
			'month':'十月份',
		},
	},
	{
		'id':'41',
		'url':'../plants/month/nov19.html',
		'year':'2019',
		'image':'month/nov.jpg',
		'en':{
			'month':"November",
		},
		'tc':{
			'month':'十一月份',
		},
		'sc':{
			'month':'十一月份',
		},
	},
	{
		'id':'42',
		'url':'../plants/month/dec19.html',
		'year':'2019',
		'image':'month/dec.jpg',
		'en':{
			'month':"December",
		},
		'tc':{
			'month':'十二月份',
		},
		'sc':{
			'month':'十二月份',
		},
	},
	{
		'id':'43',
		'url':'../plants/month/jan20.html',
		'year':'2020',
		'image':'month/jan.jpg',
		'en':{
			'month':"January",
		},
		'tc':{
			'month':'一月份',
		},
		'sc':{
			'month':'一月份',
		},
	},
	{
		'id':'44',
		'url':'../plants/month/feb20.html',
		'year':'2020',
		'image':'month/feb.jpg',
		'en':{
			'month':"February",
		},
		'tc':{
			'month':'二月份',
		},
		'sc':{
			'month':'二月份',
		},
	},
	{
		'id':'45',
		'url':'../plants/month/mar20.html',
		'year':'2020',
		'image':'month/mar.jpg',
		'en':{
			'month':"March",
		},
		'tc':{
			'month':'三月份',
		},
		'sc':{
			'month':'三月份',
		},
	},
	{
		'id':'46',
		'url':'../plants/month/apr20.html',
		'year':'2020',
		'image':'month/apr.jpg',
		'en':{
			'month':"April",
		},
		'tc':{
			'month':'四月份',
		},
		'sc':{
			'month':'四月份',
		},
	},
	{
		'id':'47',
		'url':'../plants/month/may20.html',
		'year':'2020',
		'image':'month/may.jpg',
		'en':{
			'month':"May",
		},
		'tc':{
			'month':'五月份',
		},
		'sc':{
			'month':'五月份',
		},
	},
	{
		'id':'48',
		'url':'../plants/month/jun20.html',
		'year':'2020',
		'image':'month/jun.jpg',
		'en':{
			'month':"June",
		},
		'tc':{
			'month':'六月份',
		},
		'sc':{
			'month':'六月份',
		},
	},
	{
		'id':'49',
		'url':'../plants/month/jul20.html',
		'year':'2020',
		'image':'month/jul.jpg',
		'en':{
			'month':"July",
		},
		'tc':{
			'month':'七月份',
		},
		'sc':{
			'month':'七月份',
		},
	},
	{
		'id':'50',
		'url':'../plants/month/aug20.html',
		'year':'2020',
		'image':'month/aug.jpg',
		'en':{
			'month':"August",
		},
		'tc':{
			'month':'八月份',
		},
		'sc':{
			'month':'八月份',
		},
	},
	{
		'id':'51',
		'url':'../plants/month/sep20.html',
		'year':'2020',
		'image':'month/sep.jpg',
		'en':{
			'month':"September",
		},
		'tc':{
			'month':'九月份',
		},
		'sc':{
			'month':'九月份',
		},
	},
	{
		'id':'52',
		'url':'../plants/month/oct20.html',
		'year':'2020',
		'image':'month/oct.jpg',
		'en':{
			'month':"October",
		},
		'tc':{
			'month':'十月份',
		},
		'sc':{
			'month':'十月份',
		},
	},
	{
		'id':'53',
		'url':'../plants/month/nov20.html',
		'year':'2020',
		'image':'month/nov.jpg',
		'en':{
			'month':"November",
		},
		'tc':{
			'month':'十一月份',
		},
		'sc':{
			'month':'十一月份',
		},
	},
	{
		'id':'54',
		'url':'../plants/month/dec20.html',
		'year':'2020',
		'image':'month/dec.jpg',
		'en':{
			'month':"December",
		},
		'tc':{
			'month':'十二月份',
		},
		'sc':{
			'month':'十二月份',
		},
	},
	{
		'id':'55',
		'url':'../plants/month/jan21.html',
		'year':'2021',
		'image':'month/jan.jpg',
		'en':{
			'month':"January",
		},
		'tc':{
			'month':'一月份',
		},
		'sc':{
			'month':'一月份',
		},
	},
	{
		'id':'56',
		'url':'../plants/month/feb21.html',
		'year':'2021',
		'image':'month/feb.jpg',
		'en':{
			'month':"February",
		},
		'tc':{
			'month':'二月份',
		},
		'sc':{
			'month':'二月份',
		},
	},
	{
		'id':'57',
		'url':'../plants/month/mar21.html',
		'year':'2021',
		'image':'month/mar.jpg',
		'en':{
			'month':"March",
		},
		'tc':{
			'month':'三月份',
		},
		'sc':{
			'month':'三月份',
		},
	},
	{
		'id':'58',
		'url':'../plants/month/apr21.html',
		'year':'2021',
		'image':'month/apr.jpg',
		'en':{
			'month':"April",
		},
		'tc':{
			'month':'四月份',
		},
		'sc':{
			'month':'四月份',
		},
	},
	{
		'id':'59',
		'url':'../plants/month/may21.html',
		'year':'2021',
		'image':'month/may.jpg',
		'en':{
			'month':"May",
		},
		'tc':{
			'month':'五月份',
		},
		'sc':{
			'month':'五月份',
		},
	},
	{
		'id':'60',
		'url':'../plants/month/jun21.html',
		'year':'2021',
		'image':'month/jun.jpg',
		'en':{
			'month':"June",
		},
		'tc':{
			'month':'六月份',
		},
		'sc':{
			'month':'六月份',
		},
	},
	{
		'id':'61',
		'url':'../plants/month/jul21.html',
		'year':'2021',
		'image':'month/jul.jpg',
		'en':{
			'month':"July",
		},
		'tc':{
			'month':'七月份',
		},
		'sc':{
			'month':'七月份',
		},
	},
	{
		'id':'62',
		'url':'../plants/month/aug21.html',
		'year':'2021',
		'image':'month/aug.jpg',
		'en':{
			'month':"August",
		},
		'tc':{
			'month':'八月份',
		},
		'sc':{
			'month':'八月份',
		},
	},
	{
		'id':'63',
		'url':'../plants/month/sep21.html',
		'year':'2021',
		'image':'month/sep.jpg',
		'en':{
			'month':"September",
		},
		'tc':{
			'month':'九月份',
		},
		'sc':{
			'month':'九月份',
		},
	},
	{
		'id':'64',
		'url':'../plants/month/oct21.html',
		'year':'2021',
		'image':'month/oct.jpg',
		'en':{
			'month':"October",
		},
		'tc':{
			'month':'十月份',
		},
		'sc':{
			'month':'十月份',
		},
	},
	{
		'id':'65',
		'url':'../plants/month/nov21.html',
		'year':'2021',
		'image':'month/nov.jpg',
		'en':{
			'month':"November",
		},
		'tc':{
			'month':'十一月份',
		},
		'sc':{
			'month':'十一月份',
		},
	},
	{
		'id':'66',
		'url':'../plants/month/dec21.html',
		'year':'2021',
		'image':'month/dec.jpg',
		'en':{
			'month':"December",
		},
		'tc':{
			'month':'十二月份',
		},
		'sc':{
			'month':'十二月份',
		},
	},
	{
		'id':'67',
		'url':'../plants/month/jan22.html',
		'year':'2022',
		'image':'month/jan.jpg',
		'en':{
			'month':"January",
		},
		'tc':{
			'month':'一月份',
		},
		'sc':{
			'month':'一月份',
		},
	},
	{
		'id':'68',
		'url': '../plants/month/feb22.html',
		'year':'2022',
		'image':'month/feb.jpg',
		'en':{
			'month':"February",
		},
		'tc':{
			'month':'二月份',
		},
		'sc':{
			'month':'二月份',
		},
	},
	{
		'id':'69',
		'url':'../plants/month/mar22.html',
		'year':'2022',
		'image':'month/mar.jpg',
		'en':{
			'month':"March",
		},
		'tc':{
			'month':'三月份',
		},
		'sc':{
			'month':'三月份',
		},
	},
	{
		'id':'70',
		'url':'../plants/month/apr22.html',
		'year':'2022',
		'image':'month/apr.jpg',
		'en':{
			'month':"April",
		},
		'tc':{
			'month':'四月份',
		},
		'sc':{
			'month':'四月份',
		},
	},
	{
		'id':'71',
		'url':'../plants/month/may22.html',
		'year':'2022',
		'image':'month/may.jpg',
		'en':{
			'month':"May",
		},
		'tc':{
			'month':'五月份',
		},
		'sc':{
			'month':'五月份',
		},
	},
	{
		'id':'72',
		'url':'../plants/month/jun22.html',
		'year':'2022',
		'image':'month/jun.jpg',
		'en':{
			'month':"June",
		},
		'tc':{
			'month':'六月份',
		},
		'sc':{
			'month':'六月份',
		},
	},
	{
		'id':'73',
		'url':'../plants/month/jul22.html',
		'year':'2022',
		'image':'month/jul.jpg',
		'en':{
			'month':"July",
		},
		'tc':{
			'month':'七月份',
		},
		'sc':{
			'month':'七月份',
		},
	},
	{
		'id':'74',
		'url':'../plants/month/aug22.html',
		'year':'2022',
		'image':'month/aug.jpg',
		'en':{
			'month':"August",
		},
		'tc':{
			'month':'八月份',
		},
		'sc':{
			'month':'八月份',
		},
	},
	{
		'id':'75',
		'url':'../plants/month/sep22.html',
		'year':'2022',
		'image':'month/sep22.jpg',
		'en':{
			'month':"September",
		},
		'tc':{
			'month':'九月份',
		},
		'sc':{
			'month':'九月份',
		},
	},
	{
		'id':'76',
		'url':'../plants/month/oct22.html',
		'year':'2022',
		'image':'month/oct.jpg',
		'en':{
			'month':"October",
		},
		'tc':{
			'month':'十月份',
		},
		'sc':{
			'month':'十月份',
		},
	},
	{
		'id':'77',
		'url':'../plants/month/nov22.html',
		'year':'2022',
		'image':'month/nov.jpg',
		'en':{
			'month':"November",
		},
		'tc':{
			'month':'十一月份',
		},
		'sc':{
			'month':'十一月份',
		},
	},
	{
		'id':'78',
		'url':'../plants/month/dec22.html',
		'year':'2022',
		'image':'month/dec.jpg',
		'en':{
			'month':"December",
		},
		'tc':{
			'month':'十二月份',
		},
		'sc':{
			'month':'十二月份',
		},
	},
];