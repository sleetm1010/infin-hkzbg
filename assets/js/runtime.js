var BASE_URL = window.location.origin + '/';
var LANG;

function init() {
	// get lang
	var m = window.location.href.match(/\/(en|tc|sc)(\/|$)/);
	if (m && m.length >= 2) {
		LANG = m[1];
	}

}

function parallax(e, target, layer) {
	var layer_coeff = 30 / layer;
	var x = ($(window).width() - target.offsetWidth) / 2 - (e.pageX - ($(window).width() / 2)) / layer_coeff;
	$(target).offset({ left: x });
};

function load_program_list(temp_type) {
	if (temp_type == null) {
		temp_type = 'temp';
	}

	var temp_html = '';
	var popup_html = '';
	var pg_type = '';
	var count_number = 0;

	if (temp_type == 'temp') {
		//Load page check url filter type
		var pg_type = window.location.href.match(/(#mammals|#birds|#reptiles)(\/|$)/);
		if (pg_type != null) {
			pg_type = pg_type[0].replace('#', '');
			$('.cate-list ' + '.' + pg_type).addClass('active')
			window.scrollTo(0, 0);
			
		} else {
			pg_type = '';
			$('.cate-list .all').addClass('active')
		}
	} else {
		pg_type = temp_type;
	};



	$.each(animals_data, function (i, v) {
		var check_type = false;


		if ($.isArray(v.type)) {
			$.each(v.type, function (i, v) {
				if (v == pg_type) {
					check_type = true;
					return false;
				}
			});
		} else {
			if (v.type == pg_type) {
				check_type = true;
			}
		}

		if (pg_type == '' || check_type) {
			temp_html += '<div class="animals-block">\
							<div class="cover" ';
			if (v.dataID) {
				temp_html += 'data-toggle="modal" data-target="#' + v.dataID + '"';
			}
			temp_html += '>';
			if (v.url !== '') {
				temp_html += '<a href="' + v.url + '">\
												<img src="../assets/images/animals/cover/'+ v.image + '" alt="animals">\
											  </a>';
			} else {
				temp_html += '<img src="../assets/images/animals/cover/' + v.image + '" alt="animals">';
			}
			temp_html += '</div>\
							<div class="info">\
								<p class="type ';
			if (v.type == 'mammals') {
				temp_html += "mammals";
			} else if (v.type == 'birds') {
				temp_html += "birds";
			} else if (v.type == 'reptiles') {
				temp_html += "reptiles";
			}
			temp_html += '">' + v.type + '</p>\
								<p class="name">'+ v[LANG].name + '</p>\
							</div>\
						</div>';

			count_number++;

		}

		if (v.popup) {
			popup_html += '<div id="' + v.dataID + '" class="modal animals" role="dialog">\
								<div class="modal-dialog modal-dialog-centered modal-lg" role="document">\
									<div class="modal-content">\
										<div class="modal-header clear">\
											<div class="close">\
												<img src="../assets/images/common/close_btn.png" class="close_btn" data-dismiss="modal" aria-label="Close">\
											</div>\
										</div>\
										<div class="modal-body">\
											<div class="left">\
												<div class="cover">\
													<img src="../assets/images/animals/cover/'+ v.image + '" alt="animals">\
												</div>\
											</div>\
											<div class="right">\
												<div class="info clear">\
													<div class="title">\
														<p class="name">'+ v[LANG].name + '</p>\
														<p class="scientific"><em>'+ v[LANG].scientific + '</em></p>\
													</div>';
			if (v.status) {
				popup_html += '<img src="/assets/images/animals/cover/' + v.status + '.png" class="status">';
			}

			popup_html += '</div>\
												<p class="type">'+ v[LANG].Class + ' ' + v[LANG].order + '</p>\
												<p class="distribution"><span class="dis_str">Distribution</span><br><span>'+ v[LANG].distribution + '</span></p>';
			if (v[LANG].video) {
				popup_html += '<div class="video">\
																		<p class="video_str">Video</p>\
																		<a href="'+ v[LANG].video + '" target="_blank">\
																			<img src="/assets/images/common/video_icon.png">\
																		</a>\
																	</div>';
			}
			popup_html += '</div>\
										</div>\
									</div>\
								</div>\
							</div>';
		}
	});

	$('#animals-list').html(temp_html);
	$(popup_html).insertAfter('.animalsPage main');

	$('.animals-block .cover').click(function () {
		var target = $(this).attr('data-target');
		target.modal();
	});

	if ($('html').is(':lang(tc)')) {
		$('.type.mammals').html('哺乳類');
		$('.type.birds').html('鳥類');
		$('.type.reptiles').html('爬行類');
		$('.dis_str').html('分布');
		$('.video_str').html('影片');
	} else if ($('html').is(':lang(sc)')) {
		$('.type.mammals').html('哺乳类');
		$('.type.birds').html('鸟类');
		$('.type.reptiles').html('爬行类');
		$('.dis_str').html('分布');
		$('.video_str').html('影片');
	}

	$('.cate-btn').click(function() {
		$('.cate-btn').removeClass('active');
		$(this).addClass('active');
	});
	
	
}



function load_month_list(temp_year) {

	if (temp_year == null) {
		temp_year = 'temp';
	}

	var temp_html = '';
	var pg_year = '';
	var count_number = 0;

	if (temp_year == 'temp') {
		//Load page check url filter year
		var pg_year = window.location.href.match(/(#2022|#2021|#2020|#2019|#2018|#2017|#2016)(\/|$)/);
		if (pg_year != null) {
			pg_year = pg_year[0].replace('#', '');
			// $('#month-nav .month-btn' + '.yr' + pg_year).classList.add('active')
			$('#month-nav .yr' + pg_year).addClass('active')
			
			console.log(pg_year);
		} else {
			pg_year = '';
			return
		}
	} else {
		pg_year = temp_year;
		temp_html='';
	};

	$.each(month_data, function (i, v) {
		var check_year = false;


		if ($.isArray(v.year)) {
			$.each(v.year, function (i, v) {
				if (v == pg_year) {
					check_year = true;
					return false;
				}
			});
		} else {
			if (v.year == pg_year) {
				check_year = true;
			}
		}

		if (pg_year == '' || check_year) {
			temp_html += '<div class="month-block">\
							<div class="cover">';
			if (v.url !== '') {				
				temp_html +='<a href="'+ v.url + '">\
							<img src="/assets/images/plants/'+ v.image + '" alt="flower">\
								</a>';
			}					
			else{
				temp_html += '<img src="/assets/images/plants/'+ v.image + '" alt="flower">';
			}					
			temp_html += '</div>\
			<div class="info">\
				<p class="month">'+ v[LANG].month + '</p>\
			</div>\
			</div>';
			
			count_number++;

		} 
	});

	$('#flower-list').html(temp_html);
}
function load_map(temp_type) {

	$('#map').draggable({

	});

	$('#filter .filter-block p').click(function () {
		$(this).parent().toggleClass('active');
		$(this).parent().toggleClass('open_filter_menu');
		$(this).parent().find('.filter_menu').slideToggle();
		$(this).parent().siblings().children().next().slideUp();
		return false;
	});


	$.each(map_data, function (i, v) {

		var $pt = $('<div class="map-point" data-id="' + v.id + '" data-target="' + v.filters + '"><img src="../assets/images/map/' + v.image + '" alt="point"></a>');

		$pt
			.css("top", map_data[i].top + "%")
			.css("left", map_data[i].left + "%")
			.appendTo('#map');

	})

	$('#filter .filter_menu a').click(function () {

		var target = $(this).attr('data-target');
		$(this).toggleClass("active");

		$("#map").find("[data-target='" + target + "']").toggleClass('show');

	});

	var map_scale = 1;

	if ($(window).width() < 1024) {
		map_scale = 1.9;
	}

	$('#map').css('transform', 'translate(-50%,-50%) scale(' + map_scale + ')');

	$('.btn-scale-up').click(function () {
		map_scale += 0.3;
		$('#map').css('transform', 'translate(-50%,-50%) scale(' + map_scale + ')');
	});

	$('.btn-scale-down').click(function () {
		map_scale -= 0.3;
		$('#map').css('transform', 'translate(-50%,-50%) scale(' + map_scale + ')');
	});
}
function equalHeight(group) {
	var tallest = 0;
	group.each(function () {
		var thisHeight = $(this).height();
		if (thisHeight > tallest) {
			tallest = thisHeight;
		}
	});
	group.height(tallest);
}

function adjust_main_margin() {
	//Main add margin top(Header Height)
	$('main').css('padding-top', $('header').height());
}

function rotateTable() {
	var newTable = [];
	var newTableStruct = "<tr>";
	var tableslide = $("table.roving-table").closest('.swiper-slide');

	$("table.roving-table").each(function () {
		var tableslide = $(this).closest('.swiper-slide');
		$(this).clone().addClass('mobile-show').appendTo(tableslide);
	})

	$("table.roving-table.mobile-show").find("th, td").each(function () {
		var trIndex = $(this).closest("table").find("tr").index($(this).closest("tr"));
		var tdIndex = $(this).closest("tr").find("td, th").index($(this))
		newTable[tdIndex] = newTable[tdIndex] || [];
		newTable[tdIndex][trIndex] = $(this).html();
	});

	for (i = 0; i < newTable.length; i++) {
		for (j = 0; j < newTable[0].length; j++) {
			newTableStruct += "<td>" + newTable[i][j] + "</td>";
		}
		newTableStruct += "</tr><tr>";
	}
	newTableStruct += "</tr>";
	$("table.roving-table.mobile-show").empty().append(newTableStruct);
}

function addOverlayForLoading(type) {
	switch (type) {
		case "open":
			$('body').append('<div class="loading-overlay"><img src="' + BASE_URL + 'assets/images/common/loading-frames.png" class="frame" alt="loading-frames"><div class="logo"><img src="' + BASE_URL + 'assets/images/common/loading-logo.png" class="logo-img" alt="loading"></div></div>');
			$('body').addClass('loading-overlay-open');
			$("main").hide();
			break;
		case "close":
			$('.loading-overlay').fadeOut(750);
			setTimeout(function () {
				//$('.loading-overlay').remove();
			}, 1250);
			$('body').removeClass('loading-overlay-open');
			$("main").show();
			break;
		default:
			break;
	}
}
$(document).ready(function () {

	init();

	if ($('.animalsPage').length != 0) {
		load_program_list();

		$('.cate-btn').each(function () {
			var thisHref = ($(this).attr('href').split('#'))[1];
			if (window.location.href.indexOf(thisHref) > -1) {
				$('.cate-btn.all').removeClass('active');
				$(this).addClass('active');
			}
		});
	}
	if ($('.monthPage').length != 0) {
		load_month_list();

		$('#month-nav .month-btn').click(function () {
			// $('.month-btn').removeClass('active');
			// $(this).addClass('active');
			$('#month-nav .month-btn').removeClass('active')
			let hash = this.hash
			$('yr'+hash).addClass('active')

		});

		// $('.month-btn:nth-child(3)').trigger('click');
	}
	rotateTable();
	if ($('.mapPage').length != 0) {
		load_map();
	}
	// addOverlayForLoading('open');
	
	// setTimeout(function() {
	// 	addOverlayForLoading('close');
	// }, 2500); 

	$(".back-to-top").click(function () {
		$("html, body").animate({ scrollTop: 0 }, 1000);
	});
	$('main').mousemove(function (e) {
		parallax(e, document.getElementById('moving-bg'), 2);
	});

	var $img = $('.main-banner img'), counter = 1;
	var $obj = $('.main-banner')
	setInterval(function () {
		$obj.find('> img.active').removeClass('active');
		$img.eq(counter++ % $img.length).addClass('active');
	}, 2500);



	//Smooth Scroll for "#"" elementsW scroll to correct positions
	$("a.toAnchor").on('click', function (event) {
		if (this.hash !== "") {
			event.preventDefault();
			let hash = this.hash;
			$('html, body').animate({
				scrollTop: $(hash).offset().top
			}, 400, function () {
				window.location.hash = hash;
			});
		}
	});

	$(window).on('hashchange',function(){ 
		window.location.reload(true); 
	});
});
$(function () {
	//Loading Overlay
	// addOverlayForLoading('open');
	// setTimeout(function() {
	// 	addOverlayForLoading('close');
	// }, 750); 

	//Common init
	$('header').load(BASE_URL + LANG + '/header.html', function () {

		$('header .menu #open-menu-btn').click(function () {
			$(this).toggleClass('active');
			$('header .menu').toggleClass('open_menu');
			$('header .right .open-menu').slideToggle();
			if ($(this).hasClass('active')) {
				$(this).attr('src', '/assets/images/common/cross_btn.png');
			} else {
				$(this).attr('src', '/assets/images/common/icon_menu_v2.png');
			}
		});
		$('header .open-menu .menu-block').click(function () {
			$(this).toggleClass('active');
			$(this).toggleClass('open_sub_menu');
			$(this).find('.sub-menu-block').slideToggle();
		});
	});
	$('footer').load(BASE_URL + LANG + '/footer.html', function () {
		//Share
		var $windowCount = 0;
		$('.share:not(.rss)').click(function (event) {
			event.preventDefault();
			var $thisBtn = $(this);
			var pageShareTitle = encodeURIComponent(document.getElementsByTagName("title")[0].innerHTML);
			var pageShareContent = window.location.href;
			var href = "";
			var type = 'default';
			var pageUrl = encodeURIComponent(window.location.href);
			//var pageUrl = encodeURIComponent('https://www.ogcio.gov.hk' + window.location.pathname);
			if ($thisBtn.hasClass('fb')) {
				href = "https://www.facebook.com/sharer/sharer.php?u=" + pageUrl;
			} else if ($thisBtn.hasClass('email')) {
				href = "mailto:?subject=" + pageShareTitle + "&body=" + pageShareTitle + '%0A' + pageUrl;
			} else if ($thisBtn.hasClass('wa')) {
				if ($(window).innerWidth() <= 1024) {
					href = "whatsapp://send?text=" + pageShareTitle + ": " + pageUrl;
				} else {
					href = "https://web.whatsapp.com/send?text=" + pageShareTitle + ": " + pageUrl;
				}
			} else if ($thisBtn.hasClass('tw')) {
				href = "http://twitter.com/share?text=" + pageShareTitle + ": " + "&url=" + pageUrl;
			} else if ($thisBtn.hasClass('wb')) {
				href = "http://service.weibo.com/share/share.php?url=" + pageUrl + "&title=" + pageShareTitle;
			}

			PopupCenter(href, "shareWindow_" + $windowCount, 600, 500, type);
			$windowCount++;
		});
	});


	// Nav menu 
	$('.nav-menu .nav-btn:first-child').addClass('active');
	$('.nav-content-list .nav-content:nth-child(1)').addClass('active');

	$('.nav-menu div.nav-btn').click(function () {
		//Add active class to button
		$(this).closest('.nav-menu').find('.nav-btn').removeClass('active');
		$(this).addClass('active');

		//Add active class to content
		$(this).closest('.nav-menu').parent().find('.nav-content').removeClass('active');
		$(this).closest('.nav-menu').parent().find('.nav-content[data-nav-id="' + $(this).attr('data-nav-id') + '"]').addClass('active');
	});

	//Zoom images
	var zoom_images = document.querySelectorAll('.zoom');
	for (var i = 0; i < zoom_images.length; i++) {
		zoom_images[i].addEventListener('mousemove', function (e) {
			zoom(e);
		});
	}

});
