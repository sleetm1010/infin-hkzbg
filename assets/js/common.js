function replace_url_lang(url, lang){
  if (lang != 'tc' && lang != 'sc') lang = 'en';
  return url.replace(/\/(en|tc|sc)(\/|$)/,'/'+lang+'/');
}

function change_lang(lang){
	window.location = replace_url_lang(window.location.href, lang);
}

function change_font_size(type){
	var s = 16;
	switch(type){
		case 's':
			s = 14;
			break;
		case 'm':
			s = 16;
			break;
		case 'l':
			s = 18;
			break;
	}
	$('body').css('font-size', s + 'px');
}

function scrollToObj(obj, callback) {
	var header_height = $('.top-header').outerHeight();
	
	var adjust_size = 10;
	
	if($(window).width() > 992){
		adjust_size = 10;
	}else{
		adjust_size = 80;
	}
	
	if ($(obj).length <= 0) return;
	$('body').animate({
		scrollTop: $(obj).offset().top - adjust_size
	}, 500, callback);
	$('html').animate({
		scrollTop: $(obj).offset().top - adjust_size
	}, 500);
}

function scrollToId(id){
	$obj = $('#' + id);
	scrollToObj($obj);
}

function scrollTop(){
	$('body').animate({
		scrollTop: 0
	}, 200, function() {
        
    });
	$('html').animate({
		scrollTop: 0
	}, 200);
}

function getUrlParameter(sParam) {
	var sPageURL = window.location.search.substring(1),
		sURLVariables = sPageURL.split('&'),
		sParameterName,
		i;

	for (i = 0; i < sURLVariables.length; i++) {
		sParameterName = sURLVariables[i].split('=');

		if (sParameterName[0] === sParam) {
			return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
		}
	}
};
function addOverlayForLoading(type){
	switch(type){
		case "open":
			$('body').append('<div class="loading-overlay"><img src="assets/images/common/loading.gif" alt="loading"></div>');
			$('body').addClass('loading-overlay-open');
		break;
		case "close":
			$('.loading-overlay').fadeOut(750);
			setTimeout(function() {
				//$('.loading-overlay').remove();
			},1250);
			$('body').removeClass('loading-overlay-open');
		break;
		default:
		break;
	}
}
function htmlEncode(value){
	return $('<div/>').text(value).html();
}
function PopupCenter(url, title, w, h, type) {
    if (type == 'wechat') {
        var html = $('.wc-qrcode').html();
		var wechatTitle = getTxtByLang(['WeChat Share', '微信分享', '微信分享']);
        var newWindow = window.open(url, '_blank', 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left); //　
        newWindow.document.write('<!DOCTYPE html><html><head><title>' + wechatTitle + '</title></head><body></body></html>');
        newWindow.document.write(html);
    } else {
        // Fixes dual-screen position                         Most browsers      Firefox
        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

        var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

        var left = ((width / 2) - (w / 2)) + dualScreenLeft;
        var top = ((height / 2) - (h / 2)) + dualScreenTop;
        var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

        // Puts focus on the newWindow
        if (window.focus) {
            newWindow.focus();
        }
    }
}
function initShare(){
	$('.share-btn').on('click',function(){
		$('.share-btn-list').addClass('active');
   })
   $('.share-btn-list').on('mouseleave',function(){
		$(this).removeClass('active');
   })

   var $windowCount = 0;
   new QRCode(document.getElementById("wc-qrcode"), $('#wc-qrcode').attr('title'));
   $('.share').click(function (event) {
        event.preventDefault();
        var $thisBtn = $(this);
        var pageShareTitle = encodeURIComponent(document.getElementsByTagName("title")[0].innerHTML);
        var pageShareContent = window.location.href;
        var href = "";
        var type = 'default';
        var pageUrl = encodeURIComponent(window.location.href);
        if ($thisBtn.hasClass('fb')) {
            href = "https://www.facebook.com/sharer/sharer.php?u=" + pageUrl;
        } else if ($thisBtn.hasClass('email')) {
            href = "mailto:?subject=" + pageShareTitle + "&body=" +pageShareTitle+'%0A'+ pageUrl;
        } else if ($thisBtn.hasClass('wa')) {
            if($(window).innerWidth() <= 1024){
                href = "whatsapp://send?text=" + pageShareTitle + ": " + pageUrl;
            }else{
				href = "https://web.whatsapp.com/send?text=" + pageShareTitle + ": " + pageUrl;
            }
        } else if ($thisBtn.hasClass('tw')) {
            href = "http://twitter.com/share?text=" + pageShareTitle + ": " + "&url=" + pageUrl;
        } else if ($thisBtn.hasClass('wb')) {
            href = "http://service.weibo.com/share/share.php?url=" + pageUrl + "&title=" + pageShareTitle;
        } else if ($thisBtn.hasClass('wc')) {
            href = window.location.href;
            type = 'wechat';
        }
		
        PopupCenter(href, "shareWindow_" + $windowCount, 600, 500, type);
       $windowCount++;
    });
}

function zoom(e){
  var zoomer = e.currentTarget;
  e.offsetX ? offsetX = e.offsetX : offsetX = e.touches[0].pageX
  e.offsetY ? offsetY = e.offsetY : offsetX = e.touches[0].pageX
  x = offsetX/zoomer.offsetWidth*100
  y = offsetY/zoomer.offsetHeight*100
  zoomer.style.backgroundPosition = x + '% ' + y + '%';
}


// $(document).ready(function() {
// 	$("a.toAnchor").on('click', function(event) {
// 		if (this.hash !== "") {
// 			event.preventDefault();
// 			var hash = this.hash;
// 			$('html, body').animate({
// 				scrollTop: $(hash).offset().top
// 			}, 400, function() {
// 				window.location.hash = hash;
// 			});
// 		}
// 	});
// });